CC=gcc
DIR =includes/
CFLAGS=-c -I$(DIR) -lm

distancia: point.o main.o
	 $(CC) -o distancia point.o main.o -I. -lm

point.o: point.c 
	$(CC) $(CFLAGS) point.c 

main.o: main.c 
	$(CC) $(CFLAGS) main.c 

clean:
	rm *o 
