#include "point.h"
#include <math.h>

float distancia_uclidiana(Point p1,Point p2){
	float powdifx=pow((p2.x-p1.x),2);
	float powdify=pow((p2.y-p1.y),2);
	float powdifz=pow((p2.z-p1.z),2);
    	float distance=(float)sqrt(powdifx+powdify+powdifz);
	return distance;
}

Point punto_medio(Point p1, Point p2){
	Point medio;
	medio.x = (p1.x + p2.x)/2;
	medio.y = (p1.y + p2.y)/2;
	medio.z = (p1.z + p2.z)/2;
	return medio;
}


