#include "point.h"
#include <stdlib.h>

int main()
{
    Point p1,p2,medio;
	float x1,y1,z1,x2,y2,z2;
	printf("Ingrese el punto x1: \n");
	scanf("%f",&x1);
	printf("Ingrese el punto y1: \n");
	scanf("%f",&y1);
	printf("Ingrese el punto z1: \n");
	scanf("%f",&z1);
	printf("Ingrese el punto x2: \n");
	scanf("%f",&x2);
	printf("Ingrese el punto y2: \n");
	scanf("%f",&y2);
	printf("Ingrese el punto z2: \n");
	scanf("%f",&z2);
	system("clear");
	printf("\nSus Puntos son: P1(%f,%f,%f) \t P2(%f,%f,%f) ",x1,y1,z1,x2,y2,z2);
    	p1.x=x1;
    	p1.y=y1;
    	p1.z=z1;
    	p2.x=x2;
    	p2.y=y2;
    	p2.z=z2;
	medio = punto_medio(p1,p2);
	printf("\n****************************************************************");     
    	printf("\nLa distancia uclidiana entre los 2 puntos es: %f\n",distancia_uclidiana(p1,p2));
	printf("****************************************************************\n");
	printf("\nEl punto medio es: Pm(%.2f, %.2f, %.2f)\n",medio.x, medio.y, medio.z);
	printf("****************************************************************\n");
    return 0;
}

