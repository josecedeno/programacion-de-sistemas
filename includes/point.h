
#include <stdio.h>

typedef struct Points{
	float x;
	float y;
	float z;
}Point;

float distancia_uclidiana(Point p1,Point p2);
Point punto_medio(Point p1, Point p2);
